# passfolio-tech-challenge

Programming challenge question for Passfolio's junior front end development hiring process

Using Typescript, React, and Material UI (optional but preferred), create an online SVG "drawing" SPA (single-page application) website.



Your drawing tool will allow users to add circles, rectangles, and lines, as well as to position, size, and color them.



Your screens should be inspired by the following designs, though you can improve upon them as you like:



When launched / if empty



Question-empty.png




Once you'd added some shapes:

Question-withcontent.png




As you can guess, clicking "Add …" should add the corresponding shape to your drawing, with some default values.



For each shape, there will be a list item added, which will have different options depending on the type of shape.



You can also delete shapes.



---



Feel free to use the built in svg tags from react or any 3rd party library for the svg drawing stuff, whatever you prefer.



---



Your submission must be implemented in Typescript and use React.  Clean code is very important.



Please send a copy of your working folder (do not include your node_modules or .git subfolders though) by about your 6PM.



I should be able to run it locally using "yarn install && yarn start", so it should be setup to use all the standard react scripts.  That is, you should use create-react-app to get started.

