import React, { useState } from 'react'
import shapes from "../../../shapes";
import {
	Circle as CircleInterface,
	Rectangle as RectangleInterface,
	Line as LineInterface
} from '../../../shapes/utils';

import { Canvas as CanvasComponent, AddShapes } from "../../components";
const { containers: { ShapesRenderer, ShapesEditorRenderer }, constants: { SHAPE_NAMES } } = shapes


const Canvas = () => {
	const [circles, setCircles] = useState<CircleInterface[]>([])
	const [rectangles, setRectangles] = useState<RectangleInterface[]>([])
	const [lines, setlines] = useState<LineInterface[]>([])



	const handleDeleteShape = (name: string, index: number) => {
		switch (name) {
			case SHAPE_NAMES.LINE:
				const tempLines = [...lines]
				tempLines.splice(index, 1)
				setlines(tempLines)
				break;
			case SHAPE_NAMES.CIRCLE:
				const tempCircles = [...circles]
				tempCircles.splice(index, 1)
				setCircles(tempCircles)
				break;
			case SHAPE_NAMES.RECTANGLE:
				const tempRects = [...rectangles]
				tempRects.splice(index, 1)
				setRectangles(tempRects)
				break;
			default:
				break;
		}
	}

	const handleAddShape = (name: string) => {
		switch (name) {
			case SHAPE_NAMES.LINE:
				const tempLines = [...lines]
				tempLines.push({
					x1: 8,
					y1: 14,
					x2: 63,
					y2: 98,
					thickness: 5,
					fill: "red"
				})
				setlines(tempLines)

				break;
			case SHAPE_NAMES.CIRCLE:
				const tempCircles = [...circles]
				tempCircles.push({
					x: 50,
					y: 50,
					fill: "pink",
					radius: 10
				})
				setCircles(tempCircles)
				break;
			case SHAPE_NAMES.RECTANGLE:
				const tempRects = [...rectangles]
				tempRects.push({
					x: 50,
					y: 10,
					fill: "blue",
					height: 20,
					width: 14
				})
				setRectangles(tempRects)
				break;

			default:
				break;
		}
	}

	const handleEditShape = (name: string, index: number, propKey: string, value: number | string) => {
		switch (name) {
			case SHAPE_NAMES.LINE:
				const tempLines = [...lines]
				const singleLine = tempLines[index]
				singleLine[propKey] = value
				tempLines[index] = singleLine
				setlines(tempLines)

				break;
			case SHAPE_NAMES.CIRCLE:
				const tempCircles = [...circles]
				const singleCircle = tempCircles[index]
				singleCircle[propKey] = value
				tempCircles[index] = singleCircle
				setCircles(tempCircles)
				break;
			case SHAPE_NAMES.RECTANGLE:
				const tempRects = [...rectangles]
				const singleRect = tempRects[index]
				singleRect[propKey] = value
				tempRects[index] = singleRect
				setRectangles(tempRects)
				break;

			default:
				break;
		}
	}


	const canvasWidth = window.innerWidth * .9

	return (
		<>
			{
				// TODO make responsive
			}
			<CanvasComponent height={400} width={canvasWidth} fill={"#ddd"} >
				<ShapesRenderer
					circles={circles}
					rectangles={rectangles}
					lines={lines}
				/>
			</CanvasComponent>
			<AddShapes handleAdd={handleAddShape} />
			<h1>
				Shapes
			</h1>
			<ShapesEditorRenderer
				circles={circles}
				rectangles={rectangles}
				lines={lines}
				handleDeleteShape={handleDeleteShape}
				handleEditShape={handleEditShape}
			/>
		</>
	)
}

export default Canvas
