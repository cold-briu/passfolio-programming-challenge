import React from 'react'

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import { Canvas } from "../../containers";

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		justifyContent: 'center',
		alignContent: 'center',
		padding: theme.spacing(5),
	},
	paper: {
		padding: theme.spacing(2),
	},
}));

const Home = () => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Grid item xs={12} >
				<Paper className={classes.paper}>
					<Canvas />
				</Paper>
			</Grid>
		</div>

	)
}

export default Home
