import * as views from "./views";
import * as containers from "./containers";
import * as components from "./components";

export default {
	views,
	containers,
	components,
}