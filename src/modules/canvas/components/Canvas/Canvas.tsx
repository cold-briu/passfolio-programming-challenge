import React from 'react'

interface CanvasProps {
	height: number;
	width: number;
	fill: string;
}

const Canvas: React.FC<CanvasProps> = ({
	height,
	width,
	fill,
	children
}) => {
	return (
		<svg width={width} height={height} viewBox={`0 0 ${width} ${height}`}>
			<rect x="0" y="0" width="100%" height="109%" fill={fill} />
			{
				children
			}
		</svg>)
}

export default Canvas
