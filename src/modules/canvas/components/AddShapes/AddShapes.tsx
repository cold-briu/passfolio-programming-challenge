import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Add } from '@material-ui/icons';

import shapes from "../../../shapes";

const { constants: { SHAPE_NAMES } } = shapes

interface AddShapesProps {
	handleAdd: (name: string) => void;
}

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		flexGrow: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignContent: 'center',
		padding: theme.spacing(5),
	},
	button: {
		margin: theme.spacing(1),
	},
}));

const AddShapes: React.FC<AddShapesProps> = ({ handleAdd }) => {

	const handleAddCircle = () => handleAdd(SHAPE_NAMES.CIRCLE)
	const handleAddRect = () => handleAdd(SHAPE_NAMES.RECTANGLE)
	const handleAddLine = () => handleAdd(SHAPE_NAMES.LINE)

	const classes = useStyles();
	return (
		<div className={classes.root}>
			<Button
				variant="contained"
				color="primary"
				className={classes.button}
				startIcon={<Add />}
				onClick={handleAddLine}
			>
				Add Line
      		</Button>
			<Button
				variant="contained"
				color="primary"
				className={classes.button}
				startIcon={<Add />}
				onClick={handleAddCircle}
			>
				Add Circle
      		</Button>
			<Button
				variant="contained"
				color="primary"
				className={classes.button}
				startIcon={<Add />}
				onClick={handleAddRect}
			>
				Add Rectangle
      		</Button>
		</div>
	)
}

export default AddShapes
