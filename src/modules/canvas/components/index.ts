import Canvas from "./Canvas/Canvas";
import AddShapes from "./AddShapes/AddShapes";

export {
	Canvas,
	AddShapes,
}