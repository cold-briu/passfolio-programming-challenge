import ShapesRenderer from "./ShapesRenderer/ShapesRenderer";
import ShapesEditorRenderer from "./ShapesEditorRenderer/ShapesEditorRenderer";

export {
	ShapesRenderer,
	ShapesEditorRenderer
}