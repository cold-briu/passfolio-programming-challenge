import React from 'react'

import {
	Circle as CircleInterface,
	Rectangle as RectangleInterface,
	Line as LineInterface
} from "../../utils";

import {
	SHAPE_NAMES
} from "../../constants";

import {
	CircleEditor,
	RectangleEditor,
	LineEditor,
} from "../../components";

interface ShapesRendererProps {
	circles: CircleInterface[]
	rectangles: RectangleInterface[]
	lines: LineInterface[]
	handleDeleteShape: (name: string, i: number) => void
	handleEditShape: (name: string, index: number, propKey: string, value: number | string) => void
}

const ShapesEditorRenderer: React.FC<ShapesRendererProps> = ({
	circles,
	rectangles,
	lines,
	handleDeleteShape,
	handleEditShape,
}) => {

	const handleDeleteLine = (i: number) => handleDeleteShape(SHAPE_NAMES.LINE, i)
	const handleDeleteRect = (i: number) => handleDeleteShape(SHAPE_NAMES.RECTANGLE, i)
	const handleDeleteCircle = (i: number) => handleDeleteShape(SHAPE_NAMES.CIRCLE, i)

	const handleEditLine = (i: number, propKey: string, value: number | string) => handleEditShape(SHAPE_NAMES.LINE, i, propKey, value)
	const handleEditRect = (i: number, propKey: string, value: number | string) => handleEditShape(SHAPE_NAMES.RECTANGLE, i, propKey, value)
	const handleEditCircle = (i: number, propKey: string, value: number | string) => handleEditShape(SHAPE_NAMES.CIRCLE, i, propKey, value)

	return (
		<>
			{
				lines.map((lineData, i) => <LineEditor
					index={i}
					properties={lineData}
					handleDelete={handleDeleteLine}
					handleEdit={handleEditLine}
				/>)
			}
			{
				rectangles.map((rectangleData, i) => <RectangleEditor
					index={i}
					properties={rectangleData}
					handleDelete={handleDeleteRect}
					handleEdit={handleEditRect}
				/>)
			}
			{
				circles.map((circleData, i) => <CircleEditor
					index={i}
					properties={circleData}
					handleDelete={handleDeleteCircle}
					handleEdit={handleEditCircle}
				/>)
			}
		</>
	)
}

export default ShapesEditorRenderer
