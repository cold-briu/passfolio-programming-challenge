import React from 'react'

import {
	Circle as CircleInterface,
	Rectangle as RectangleInterface,
	Line as LineInterface
} from "../../utils";

import {
	Circle,
	Rectangle,
	Line
} from "../../components";

interface ShapesRendererProps {
	circles: CircleInterface[]
	rectangles: RectangleInterface[]
	lines: LineInterface[]
}

const ShapesRenderer: React.FC<ShapesRendererProps> = ({
	circles,
	rectangles,
	lines
}) => {


	return (
		<>
			{
				lines.map((lineData) => <Line {...lineData} />)
			}
			{
				rectangles.map((rectangleData) => <Rectangle {...rectangleData} />)
			}
			{
				circles.map((circleData) => <Circle {...circleData} />)
			}
		</>
	)
}

export default ShapesRenderer
