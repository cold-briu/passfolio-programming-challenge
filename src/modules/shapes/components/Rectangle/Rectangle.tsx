import React from 'react'
import { Rectangle as RectInterface } from '../../utils'

interface RectProps extends RectInterface { }

const Rectangle: React.FC<RectProps> = ({
	x,
	y,
	height,
	width,
	fill
}) => {
	return (
		<svg viewBox="0 0 100 100" >
			<rect x={x} y={y} width={width} height={height} fill={fill} />
		</svg>

	)
}

export default Rectangle
