import React from 'react'
import { Line as LineInterface } from '../../utils'

interface LineProps extends LineInterface { }

const Line: React.FC<LineProps> = ({
	x1,
	y1,
	x2,
	y2,
	thickness,
	fill
}) => {
	return (
		<svg viewBox="0 0 100 100" >
			<line x1={x1} y1={y1} x2={x2} y2={y2} style={{ "stroke": fill, "strokeWidth": thickness }} />
		</svg>
	)
}

export default Line
