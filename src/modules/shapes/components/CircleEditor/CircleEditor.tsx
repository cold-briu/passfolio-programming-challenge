import React, { useState } from 'react'
import { Circle as CircleInterface } from '../../utils'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


interface CircleEditorProps {
	index: number;
	properties: CircleInterface;
	handleDelete: (i: number) => void;
	handleEdit: (i: number, propKey: string, value: number | string) => void
}

const useStyles = makeStyles((theme) => ({
	button: {
		margin: theme.spacing(1),
	},
}));

const CircleEditor: React.FC<CircleEditorProps> = ({
	index,
	properties,
	handleDelete
}) => {
	const classes = useStyles();
	const deleteShape = () => handleDelete(index)

	const [x, setX] = useState(properties.x)
	const [y, setY] = useState(properties.y)
	const [radius, setRadius] = useState(properties.radius)
	const [fill, setFill] = useState(properties.fill)


	return (
		<>
			<h2>Circle {index + 1}</h2>
			<span>
				<p>Fill:</p>
				<input type="text" value={fill} />
			</span>
			<span>
				<p>Left:</p>
				<input type="text" value={x} />
			</span>
			<span>
				<p>Top:</p>
				<input type="text" value={y} />
			</span>
			<span>
				<p>Radius:</p>
				<input type="text" value={radius} />
			</span>
			<Button
				variant="contained"
				color="secondary"
				className={classes.button}
				startIcon={<DeleteIcon />}
				onClick={deleteShape}
			>
				Delete
      		</Button>
		</>
	)
}

export default CircleEditor
