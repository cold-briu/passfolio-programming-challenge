import React, { useState } from 'react'
import { Line as LineInterface } from '../../utils'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


interface LineEditorProps {
	index: number;
	properties: LineInterface;
	handleDelete: (i: number) => void;
	handleEdit: (i: number, propKey: string, value: number | string) => void
}

const useStyles = makeStyles((theme) => ({
	button: {
		margin: theme.spacing(1),
	},
}));

const LineEditor: React.FC<LineEditorProps> = ({
	index,
	properties,
	handleDelete
}) => {
	const classes = useStyles();

	const deleteShape = () => handleDelete(index)

	const [fill, setFill] = useState(properties.fill)


	return (
		<>
			<h2>Line {index + 1}</h2>
			<span>
				<p>Fill:</p>
				<input type="text" value={fill} />
			</span>
			<Button
				variant="contained"
				color="secondary"
				className={classes.button}
				startIcon={<DeleteIcon />}
				onClick={deleteShape}

			>
				Delete
      </Button>
		</>
	)
}

export default LineEditor
