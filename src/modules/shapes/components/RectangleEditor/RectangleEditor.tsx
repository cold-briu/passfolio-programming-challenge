import React from 'react'
import { Rectangle as RectangleInterface } from '../../utils'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


interface RectangleEditorProps {
	index: number;
	properties: RectangleInterface;
	handleDelete: (i: number) => void;
	handleEdit: (i: number, propKey: string, value: number | string) => void

}

const useStyles = makeStyles((theme) => ({
	button: {
		margin: theme.spacing(1),
	},
}));

const RectangleEditor: React.FC<RectangleEditorProps> = ({
	index,
	properties,
	handleDelete
}) => {
	const classes = useStyles();

	const deleteShape = () => handleDelete(index)


	return (
		<>
			<h2>Rectangle {index + 1}</h2>
			{ JSON.stringify(properties)}
			<Button
				variant="contained"
				color="secondary"
				className={classes.button}
				startIcon={<DeleteIcon />}
				onClick={deleteShape}

			>
				Delete
      </Button>
		</>
	)
}

export default RectangleEditor
