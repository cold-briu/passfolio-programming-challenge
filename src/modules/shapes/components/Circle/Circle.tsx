import React from 'react'
import { Circle as CircleInterface } from '../../utils'

interface CircleProps extends CircleInterface { }

const Circle: React.FC<CircleProps> = ({
	x,
	y,
	radius,
	fill,
}) => {
	return (
		<svg viewBox="0 0 100 100" >
			<circle cx={x} cy={y} r={radius} fill={fill} />
		</svg>
	)
}

export default Circle