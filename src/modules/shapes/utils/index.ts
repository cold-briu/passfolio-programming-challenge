interface accesibleObject {
	[key: string]: any;
}
export interface Shape {
	name: string;
	properties: Circle | Rectangle | Line;
}

export interface Circle extends accesibleObject {
	x: number;
	y: number;
	radius: number;
	fill: string;
}

export interface Rectangle extends accesibleObject {
	x: number;
	y: number;
	height: number;
	width: number;
	fill: string;
}

export interface Line extends accesibleObject {
	x1: number;
	y1: number;
	x2: number;
	y2: number;
	thickness: number;
	fill: string;
} 