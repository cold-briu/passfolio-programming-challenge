import React from 'react';
import { canvas } from './modules';

const { views: { Home } } = canvas

function App() {
  return (
    <>
      <Home />
    </>
  );
}

export default App;
